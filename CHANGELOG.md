# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog][] and this project adheres to
[Semantic Versioning][].

[Keep a Changelog]: http://keepachangelog.com/
[Semantic Versioning]: http://semver.org/

## [Unreleased][]
### Added
- Get ready for translations (i18n).

## [0.9.0][] - 2020-08-18
### Added
- "Created by" and "changed by" area authors.
- [WSChat](https://sr.ht/~qeef/wschat/) client.

### Changed
- Merge square and area page.
- Slightly change design.
- Do not show commits by default.

## [0.8.0][] - 2020-07-05
### Added
- Send message over OpenStreetMap to the author of the commit.

## [0.7.1][] - 2020-02-24
### Fixed
- Not showing user statistics when no commits.

## [0.7.0][] - 2020-02-14
### Added
- Delete token procedure to `lib.js` -- support for logout functionality.
- User's last week statistics.

## [0.6.0][] - 2020-02-06
### Added
- Tags to comments of iD.
- Mappers score.

### Changed
- Set `gray` as default color for *to map* everywhere.

## [0.5.5][] - 2020-02-03
### Fixed
- Wrong index of commit when loading area (found during FOSDEM 2020
  presentation).

## [0.5.4][] - 2020-02-01
### Changed
- Squares map colors.

## [0.5.3][] - 2020-01-31
### Added
- Footer.

## [0.5.2][] - 2020-01-30
### Fixed
- Order areas by the priority, descendant.

## [0.5.1][] - 2020-01-28
### Fixed
- Default title for square in squares map.

## [0.5.0][] - 2020-01-28
### Addded
- Squares map for area.

## [0.4.0][] - 2020-01-24
### Added
- Damn client library. Use API procedures from library in client.
- When square has bad geometry (less than 3 points), automatically mark as
  done.

### Changed
- Change design of area.
- Refactor area code.
- Use global variables.
- Store commits to global variables.
- Refactor square code.

### Removed
- Unused square procedures.

## [0.3.0][] - 2020-01-13
### Added
- Map and review random squares.

## [0.2.0][] - 2020-01-06
### Added
- Mapping rate and time estimation.

### Changed
- Restrict number of commits loaded when got area.
- Commits loaded independently on area details.

## 0.1.0 - 2020-01-01
### Added
- Changelog, license, readme.
- Area and square basic functions.
- Authentication to OpenStreetMap server with authorization to damn server.
- Let users translate area description.
- Map and review functionality.

[Unreleased]: https://gitlab.com/damn-project/damn_client/compare/v0.9.0...master
[0.9.0]: https://gitlab.com/damn-project/damn_client/compare/v0.8.0...v0.9.0
[0.8.0]: https://gitlab.com/damn-project/damn_client/compare/v0.7.1...v0.8.0
[0.7.1]: https://gitlab.com/damn-project/damn_client/compare/v0.7.0...v0.7.1
[0.7.0]: https://gitlab.com/damn-project/damn_client/compare/v0.6.0...v0.7.0
[0.6.0]: https://gitlab.com/damn-project/damn_client/compare/v0.5.5...v0.6.0
[0.5.5]: https://gitlab.com/damn-project/damn_client/compare/v0.5.4...v0.5.5
[0.5.4]: https://gitlab.com/damn-project/damn_client/compare/v0.5.3...v0.5.4
[0.5.3]: https://gitlab.com/damn-project/damn_client/compare/v0.5.2...v0.5.3
[0.5.2]: https://gitlab.com/damn-project/damn_client/compare/v0.5.1...v0.5.2
[0.5.1]: https://gitlab.com/damn-project/damn_client/compare/v0.5.0...v0.5.1
[0.5.0]: https://gitlab.com/damn-project/damn_client/compare/v0.4.0...v0.5.0
[0.4.0]: https://gitlab.com/damn-project/damn_client/compare/v0.3.0...v0.4.0
[0.3.0]: https://gitlab.com/damn-project/damn_client/compare/v0.2.0...v0.3.0
[0.2.0]: https://gitlab.com/damn-project/damn_client/compare/v0.1.0...v0.2.0
