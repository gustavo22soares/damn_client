/*

The damn client translations
============================

These are strings used in the damn client. Feel free to add new language:

- Copy rows 50 to 117 just before the row `COPY NEW STRINGS BEFORE THIS ROW`.

- Update row `"en": {` and row `end of "en"` with your language code.

- Translate the strings.

- Put yourself before other authors under license statement heading. (I mean --
  grow the list of authors upstairs, so the newer authors are upper (because of
  a year.))

- Create merge request for https://gitlab.com/damn-project/damn_client.

Then, use new language:

- In `f.js` file, update `LANG="en"` with your language code.


These translations are published under MIT license
--------------------------------------------------

Copyright (c) 2020 Jiri Vlasak

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*/
i18n = {
"en": {
    "Loading data": "Loading data",
    "to map": "to map",
    "to review": "to review",
    "done": "done",
    "is mapping": "is mapping",
    "Authenticate again": "Authenticate again",
    "Authenticate to OpenStreetMap": "Authenticate to OpenStreetMap",
    "Square": "Square",
    "Area": "Area",
    "commits": "commits",
    "Connected": "Connected",
    "Disconnected": "Disconnected",
    "Either not authenticated or wschat not available.": "Either not authenticated or wschat not available.",
    "translate": "translate",
    "Language code": "Language code",
    "Add translation": "Add translation",
    "I will not translate": "I will not translate",
    "Show commits": "Show commits",
    "Hide commits": "Hide commits",
    "Area created by": "Area created by",
    "changed by": "changed by",
    "We are mapping": "We are mapping",
    "You are currently on square": "You are currently on square",
    "of area": "of area",
    "Stop mapping and leave a message": "Stop mapping and leave a message",
    "This square needs more mapping": "This square needs more mapping",
    "Split this square as it is huge": "Split this square as it is huge",
    "Ready for review": "Ready for review",
    "Someone, please, review my review": "Someone, please, review my review",
    "I approve this one as completed": "I approve this one as completed",
    "Go back to area": "Go back to area",
    "I will map": "I will map",
    "random square": "random square",
    "recent square": "recent square",
    "I will review": "I will review",
    "Show chat": "Show chat",
    "Hide chat": "Hide chat",
    "Can't connect to wschat server": "Can't connect to wschat server",
    "Type a message here. Send by <Enter> key.": "Type a message here. Send by <Enter> key.",
    "Mapping rate": "Mapping rate",
    "The area is mapped in": "The area is mapped in",
    "hours when mapping": "hours when mapping",
    "The area is done in": "The area is done in",
    "hours when reviewing": "hours when reviewing",
    "squares per hour": "squares per hour",
    "squares per 3 hours": "squares per 3 hours",
    "squares per day": "squares per day",
    "squares per week": "squares per week",
    "Review rate": "Review rate",
    "Mappers score": "Mappers score",
    "Mapped": "Mapped",
    "Reviewed": "Reviewed",
    "squares": "squares",
    "Squares map": "Squares map",
    "Open iD editor again": "Open iD editor again",
    "I had to split this as no human could map it alone.": "I had to split this as no human could map it alone.",
    "This square is ready for review. I swear.": "This square is ready for review.  I swear.",
    "This square needs a little bit more mapping.": "This square needs a little bit more mapping.",
    "Please, review my review someone. Thanks.": "Please, review my review someone. Thanks.",
    "Yay! This square is completely done.": "Yay! This square is completely done.",
    "logout": "logout",
    "load areas": "load areas",
    "and last week statistics": "and last week statistics",
    "hours": "hours",
    "Total": "Total",
}, // end of "en"

"cs": {
    "Loading data": "Načítám data",
    "to map": "k mapování",
    "to review": "ke kontrole",
    "done": "hotovo",
    "is mapping": "mapuje",
    "Authenticate again": "Znovu ověřit",
    "Authenticate to OpenStreetMap": "Ověřit s OpenStreetMap",
    "Square": "Čtverec",
    "Area": "Oblast",
    "commits": "změny",
    "Connected": "Připojeno",
    "Disconnected": "Odpojeno",
    "Either not authenticated or wschat not available.": "Buď neověřený, nebo není wschat k dispozici.",
    "translate": "přelož",
    "Language code": "Kód jazyka",
    "Add translation": "Přidej překlad",
    "I will not translate": "Nebudu překládat",
    "Show commits": "Zobraz změny",
    "Hide commits": "Skryj změny",
    "Area created by": "Oblast vytvořil/a",
    "changed by": "změnil/a",
    "We are mapping": "Mapujeme",
    "You are currently on square": "Právě mapuješ čtverec",
    "of area": "oblasti",
    "Stop mapping and leave a message": "Ukončit mapování a zanechat zprávu",
    "This square needs more mapping": "Tento čtverec potřebuje více mapování",
    "Split this square as it is huge": "Rozděl tento čtverec, protože je velký",
    "Ready for review": "Připraveno ke kontrole",
    "Someone, please, review my review": "Někdo, prosím, zkontrolujte moji kontrolu",
    "I approve this one as completed": "Schvaluji jako hotový",
    "Go back to area": "Běž zpět na oblast",
    "I will map": "Budu mapovat",
    "random square": "náhodný čtverec",
    "recent square": "nejnovější čtverec",
    "I will review": "Zkontroluju",
    "Show chat": "Zobraz diskuzi",
    "Hide chat": "Skryj diskuzi",
    "Can't connect to wschat server": "Nemůžu se připojit k wschat serveru",
    "Type a message here. Send by <Enter> key.": "Zadej zprávu. Odešli klávesou <Enter>.",
    "Mapping rate": "Míra mapování",
    "The area is mapped in": "Oblast bude zmapovaná za",
    "hours when mapping": "hodin, když se zmapuje",
    "The area is done in": "Oblast bude hotová za",
    "hours when reviewing": "hodin, když se zkontroluje",
    "squares per hour": "čtverců za hodinu",
    "squares per 3 hours": "čtverců za 3 hodiny",
    "squares per day": "čtverců za den",
    "squares per week": "čtverců za týden",
    "Review rate": "Míra kontrolování",
    "Mappers score": "Výsledky mapperů",
    "Mapped": "Zmapováno",
    "Reviewed": "Zkontrolováno",
    "squares": "čtverce",
    "Squares map": "Mapa čtverců",
    "Open iD editor again": "Znovu otevřít iD editor",
    "I had to split this as no human could map it alone.": "Musel jsem to rozdělit, protože tohle by sám nezmapoval žádný člověk.",
    "This square is ready for review. I swear.": "Tenhle čtverec je připravený na kontrolu. Přísahám.",
    "This square needs a little bit more mapping.": "Tenhle čtverec potřebuje ještě trochu mapování.",
    "Please, review my review someone. Thanks.": "Prosím, zkontrolujte po mě někdo moji kontrolu. Díky.",
    "Yay! This square is completely done.": "Jupí! Tenhle čtverec je kompletně hotov.",
    "logout": "odhlásit",
    "load areas": "načti oblasti",
    "and last week statistics": "a statistiky posledního týdne",
    "hours": "hodin",
    "Total": "Celkem",
}, // end of "cs"

}; // COPY NEW STRINGS BEFORE THIS ROW

// API
function L(string)
{
    return i18n[LANG][string];
}
