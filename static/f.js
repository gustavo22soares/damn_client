// -- global library variables
DAMN_SERVER = "https://server.damn-project.org"
LANG="en"

// -- global variables
var areas = {};
var curr_aid = false;
var curr_sid = false;
const uri = "wss://chat.damn-project.org";
var ws;

// -- globals, generic --
function reset_globals()
{
    curr_aid = false;
    curr_sid = false;
    up("working", L("Loading data") + "...");
}
function curr_lcd() // Returns last commit date for current area.
{
    var lcd = "2020-01-01T00:00:00.0";
    if (curr_aid && areas[curr_aid] && areas[curr_aid]["commits"].length > 0)
        lcd = areas[curr_aid]["commits"][0]["date"];
    return lcd;
}
function show_div(id)
{
    document.getElementById(id).style.display = "block";
}
function hide_div(id)
{
    document.getElementById(id).style.display = "none";
}

// -- areas --
function html_area_card(area)
{
    var to_map = area["stats"]["% to map"];
    var to_val = area["stats"]["% to review"];
    var done = area["stats"]["% done"];
    var card = "<div";
            card += " class='card'";
            card += " onclick='goto_area(" + area["aid"] + ")'";
        card += ">";
        card += "<div class='card_top text'>";
            card += area["aid"] + " ";
            card += area["tags"];
        card += "</div>";
        card += "<div class='card_bottom'>";
            card += "<div class='card_map'>";
            card += Math.floor(to_map) + "% " + L("to map");
            card += "</div>";
            card += "<div";
                card += " class='card_review'";
                card += " style='height:" + to_val + "%;'";
            card += ">";
            card += ((to_val > 0)?(Math.floor(to_val) + 1):0);
                card += "% " + L("to review");
            card += "</div>";
            card += "<div class='card_done' style='height:" + done + "%;'>";
            card += Math.floor(done) + "% " + L("done");
            card += "</div>";
        card += "</div>";
    card += "</div>";
    return card;
}
function show_areas()
{
    var tl = [];
    for (var a in areas)
        tl.push([areas[a]["priority"], areas[a]["aid"]]);
    tl.sort(function(a, b) { return b[0] - a[0]; });
    var ih = "";
    for (var i = 0; i < tl.length; i++)
        ih += html_area_card(areas[tl[i][1]]);
    up("main", ih);
    up("working", "");
}
function store_areas(received)
{
    for (var a in received) {
        if (!areas[received[a]["aid"]])
            areas[received[a]["aid"]] = {};
        areas[received[a]["aid"]]["aid"] = received[a]["aid"];
        areas[received[a]["aid"]]["tags"] = received[a]["tags"];
        areas[received[a]["aid"]]["priority"] = received[a]["priority"];
        areas[received[a]["aid"]]["created"] = received[a]["created"];
        areas[received[a]["aid"]]["stats"] = received[a]["stats"];
        if (!areas[received[a]["aid"]]["description"])
            areas[received[a]["aid"]]["description"] = {};
        if (!areas[received[a]["aid"]]["instructions"])
            areas[received[a]["aid"]]["instructions"] = {};
        if (!areas[received[a]["aid"]]["commits"])
            areas[received[a]["aid"]]["commits"] = [];
        areas[received[a]["aid"]]["#squares"] =
            received[a]["stats"]["to map"]
            + received[a]["stats"]["to review"]
            + received[a]["stats"]["done"]
        ;
    }
}
function store_show_areas(received)
{
    store_areas(received);
    show_areas();
}
function load_areas()
{
    up("working", L("Loading data") + "...");
    get_areas(store_show_areas);

    var ih = "";
    var u = load_user();
    if (u) {
        ih += "<a href='javascript:show_user()'>";
        ih += u["display_name"];
        ih += "</a>";
        ih += " " + L("is mapping") + ".";
        ih += " <a href='javascript:authenticate()'>";
                ih += L("Authenticate again");
        ih += "</a>.";
    } else {
        ih += "<a href='javascript:authenticate()'>";
                ih += L("Authenticate to OpenStreetMap");
        ih += "</a>.";
    }

    up("user", ih);
}

// -- area --
function show_area()
{
    var ih = "";
    ih += "<div class='w37'>";
        ih += "<div id='tags' class='text'>";
            ih += html_curr_tags();
        ih += "</div>";
        ih += "<div id='instructions' class='text'>";
            ih += html_curr_instructions();
        ih += "</div>";
        ih += "<div id='actions' class='text'>";
            ih += html_curr_actions();
        ih += "</div>";
        ih += "<div id='authors' class='text'>";
            ih += html_curr_authors();
        ih += "</div>";
        ih += "<div id='commits' class=''>";
        ih += "</div>";
    ih += "</div>";
    ih += "<div id='stats' class='w62'>";
        ih += html_curr_stats();
    ih += "</div>";
    ih += "<div id='description' class='w62'>";
        ih += html_curr_description();
    ih += "</div>";
    ih += "<div id='description_tr' class='w62'>";
        ih += html_curr_description_tr();
    ih += "</div>";
    ih += "<div id='chat' class='w62'>";
        ih += html_wschat();
    ih += "</div>";
    ih += "<div id='rate' class='w62 text'>";
        ih += html_curr_rate();
    ih += "</div>";
    ih += "<div id='score' class='w62 text'>";
        ih += html_curr_score();
    ih += "</div>";
    ih += "<div id='squares_map' class='w62'>";
    ih += "</div>";
    up("main", ih);
    up("working", "");
}
function store_area(received)
{
    areas[received["aid"]]["aid"] = received["aid"];
    areas[received["aid"]]["tags"] = received["tags"];
    areas[received["aid"]]["priority"] = received["priority"];
    areas[received["aid"]]["created"] = received["created"];
    areas[received["aid"]]["description"] = received["description"];
    areas[received["aid"]]["instructions"] = received["instructions"];
}
function show_squares_map()
{
    if (!areas[curr_aid]["squares"]) {
        load_squares_map();
        return;
    }
    up("squares_map", html_squares_map());
    change_squares_map_colors();
}
function store_show_squares_map(r)
{
    areas[r["aid"]]["squares"] = r["squares"];
    show_squares_map();
}
function load_squares_map()
{
    get_ajaj(function(r) {
        store_show_squares_map(r);
    }, ep("/area/" + curr_aid + "/stats"));
}
function store_show_area(received)
{
    store_area(received);
    up("tags", html_curr_tags());
    up("instructions", html_curr_instructions());
    up("description", html_curr_description());
}
function change_square_map_color(c)
{
    var s = document.getElementById("square" + c["sid"]);
    if (!s)
        return;
    var st = document.getElementById("square" + c["sid"] + "title");
    if (!st)
        return;
    st.innerHTML = L("Square") + " " + c["sid"] + " " + c["type"];
    switch (c["type"]) {
    case "locked":
        s.style.fill = "black";
        break;
    case "to review":
        s.style.fill = "silver";
        break;
    case "done":
        s.style.fill = "white";
        break;
    case "to map":
    default:
        s.style.fill = "gray";
    }
}
function change_squares_map_colors()
{
    for (var i = areas[curr_aid]["commits"].length - 1; i >= 0; i--)
        change_square_map_color(areas[curr_aid]["commits"][i]);
}
function store_commits(r)
{
    if (r.length == 0)
        return;
    var lsm = true;
    var area_changed = false;
    for (var i = r.length - 1; i >= 0; i--) {
        if (r[i]["sid"] == null)
            area_changed = true;
        if (r[i]["sid"] > areas[curr_aid]["#squares"])
            areas[curr_aid]["#squares"] = r[i]["sid"];
        if (r[i]["message"] == "The square was splitted" && lsm) {
            lsm = false;
            load_squares_map();
        }
        areas[curr_aid]["commits"].unshift(r[i]);
        change_square_map_color(r[i]);
    }
    if (area_changed)
        get_area(store_show_area, curr_aid);
    up("authors", html_curr_authors());
    up("stats", html_curr_stats());
    up("rate", html_curr_rate());
    up("score", html_curr_score());
    up("actions", html_curr_actions());
    up("authors_h", ((curr_sid)?L("Square"):L("Area")) + " " + L("commits"));
    if (document.getElementById("commits").innerHTML != "")
        up("commits", html_curr_commits());
}
function goto_area(aid, from_square=false)
{
    window.scrollTo(0, 0);
    reset_globals();
    curr_aid = aid;
    if (!from_square) {
        show_area();
        get_area(store_show_area, curr_aid); // load area
        load_squares_map();

        /* --- wschat functions --- */
        ws = new WebSocket(uri);
        ws.onopen = function() {
            const messages = document.getElementById("messages");
            messages.innerHTML = L("Connected") + ".";
            show_div("msg_div");
        };
        ws.onmessage = function(m) {
            var msg = JSON.parse(m.data);
            if (msg["rid"] == curr_aid)
                add_msg(msg);
        };
        ws.onclose = function() {
            const messages = document.getElementById("messages");
            messages.innerHTML = L("Disconnected") + ".";
        };
        ws.onerror = function() {
            const messages = document.getElementById("messages");
            messages.innerHTML = L("Either not authenticated or wschat not available.");
            hide_div("msg_div");
        };
        /* --- */
    }
    get_commits(store_commits, curr_aid, curr_lcd());
    up("actions", html_curr_actions());
    up("authors_h", ((curr_sid)?L("Square"):L("Area")) + " " + L("commits"));
    if (document.getElementById("commits").innerHTML != "")
        up("commits", html_curr_commits());
}
// -- area -- html procedures
function html_curr_commits()
{
    var ih = "";
    for (var c in areas[curr_aid]["commits"]) {
        var commit = areas[curr_aid]["commits"][c];
        if (curr_sid && commit["sid"] != curr_sid)
            continue;
        ih += "<div class='commit'";
            if (commit["sid"]) {
                ih += " onclick='goto_square(";
                ih += commit["aid"];
                ih += ", ";
                ih += commit["sid"];
                ih += ")'";
            }
            ih += " title='";
                ih += L("Area") + " " + commit["aid"];
                ih += ", " + L("Square") + " " + commit["sid"];
                ih += ", " + commit["type"];
            ih += "'";
        ih += ">";
            ih += "<div class='commit_message text'>";
            ih += commit["message"];
            ih += "</div>";
            ih += "<div class='commit_sign text'>";
            var d = new  Date(commit["date"]);
            ih += "<a";
                ih += " onclick='event.stopPropagation()'";
                ih += " href='https://www.openstreetmap.org/message/new/";
                    ih += commit["author"];
                    ih += "'";
                ih += " target='_blank'";
            ih += ">";
                ih += commit["author"];
            ih += "</a>";
            ih += ", " + d.toLocaleString();
            ih += "</div>";
        ih += "</div>";
    }
    return ih;
}
function html_curr_tags()
{
    return areas[curr_aid]["tags"];
}
function html_curr_stats()
{
    var computed = compute_curr_stats();
    var to_map = computed["% to map"];
    var to_rev = computed["% to review"];
    var done = computed["% done"];
    var ih = "";
    ih += "<div class='stats_map'>";
    ih += Math.floor(to_map) + "% " + L("to map");
    ih += "</div>";
    ih += "<div class='stats_review' style='width:" + to_rev + "%;'>";
    ih += ((to_rev > 0) ? (Math.floor(to_rev) + 1) : 0) + "% " + L("to review");
    ih += "</div>";
    ih += "<div class='stats_done' style='width:" + done + "%;'>";
    ih += Math.floor(done) + "% " + L("done");
    ih += "</div>";
    return ih;
}
function compute_curr_stats()
{
    var added = [];
    var to_rev = 0;
    var done = 0;
    for (var i = 0; i < areas[curr_aid]["commits"].length; i++) {
        var commit = areas[curr_aid]["commits"][i];
        if (added.indexOf(commit["sid"]) != -1)
            continue;
        added.push(commit["sid"]);
        if (commit["type"] == "to review")
            to_rev += 1;
        else if (commit["type"] == "done")
            done += 1;
    }
    var s = areas[curr_aid]["#squares"];
    var perc_to_map = 100 * (s - to_rev - done) / s;
    var perc_to_rev = 100 * to_rev / s;
    var perc_done = 100 * done / s;
    return {
        "% to map": perc_to_map,
        "% to review": perc_to_rev,
        "% done": perc_done,
    }
}
function html_curr_description()
{
    var ih = "";
    ih += "<div id='description_lang'>";
    for (var l in areas[curr_aid]["description"]) {
        ih += "<div class='description_button'";
        ih += " onclick='up(";
            ih += '"description_text"';
            ih += ', "' + areas[curr_aid]["description"][l] + '"';
        ih += ")'";
        ih += ">" + l + "</div>";
    }

        ih += "<div class='description_button' onclick='display_tr()'>";
        ih += L("translate");
        ih += "</div>";
    ih += "</div>";
    ih += "<div id='description_text' class='text'>";
    for (var l in areas[curr_aid]["description"]) {
        ih += areas[curr_aid]["description"][l];
        break;
    }
    ih += "</div>";
    return ih;
}
function html_curr_description_tr()
{
    var ih = "";
    ih += "<textarea id='description_tr_lang' onclick='this.value=\"\"'>";
    ih += L("Language code");
    ih += "</textarea>";
    ih += "<textarea id='description_tr_text'></textarea>";
    ih += "<div class='button' onclick='update_tr()'>";
    ih += L("Add translation");
    ih += "</div>";
    ih += "<div class='button' onclick='hide_tr()'>";
    ih += L("I will not translate");
    ih += "</div>";
    return ih;
}
function display_tr()
{
    document.getElementById("description_tr_lang").value = L("Language code");
    document.getElementById("description_tr").style.display = "block";
}
function hide_tr()
{
    document.getElementById("description_tr").style.display = "none";
}
function update_tr()
{
    hide_tr();
    var ad = areas[curr_aid]["description"];
    var nl = document.getElementById("description_tr_lang").value;
    var nt = document.getElementById("description_tr_text").value;
    ad[nl] = nt;
    put_area(
        function (r) {
            get_area(store_show_area, curr_aid);
        },
        {
            "aid": curr_aid,
            "description": ad,
        },
    );
}
function html_curr_authors()
{
    var end = "";
    end += ".";
    end += "<br />";
    end += "<br />";
    end += "<br />";
    end += "<center>";
    end += "<a href='javascript:up(\"commits\", html_curr_commits())'>";
        end += L("Show commits");
    end += "</a>";
    end += "&nbsp;|&nbsp;";
    end += "<a href='javascript:up(\"commits\", \"\")'>";
        end += L("Hide commits");
    end += "</a>";
    end += "</center>";
    end += "<br />";
    if (areas[curr_aid]["commits"].length == 0)
        return "<h2 id='authors_h'></h2>";
    var last_i = areas[curr_aid]["commits"].length - 1;
    if (
        areas[curr_aid]["commits"][last_i]["date"]
        != areas[curr_aid]["created"]
    )
        return "<h2 id='authors_h'></h2>";
    var ih = "";
    ih += "<h2 id='authors_h'></h2>";
    ih += L("Area created by") + " ";
    var authors = [];
    authors.push(areas[curr_aid]["commits"][last_i]["author"]);
    for (var c in areas[curr_aid]["commits"]) {
        var commit = areas[curr_aid]["commits"][c];
        if (commit["sid"] == null && authors.indexOf(commit["author"]) == -1)
            authors.push(commit["author"]);
    }
    ih += "<a";
        ih += " href='https://www.openstreetmap.org/message/new/";
            ih += authors[0];
            ih += "'";
        ih += " target='_blank'";
    ih += ">";
        ih += authors[0];
    ih += "</a>";
    if (authors.length == 1)
        return ih + end;
    ih += ", " + L("changed by") + " ";
    for (var i = authors.length - 1; i > 0; i--) {
        if (i < authors.length - 1)
            ih += ", ";
        ih += "<a";
            ih += " href='https://www.openstreetmap.org/message/new/";
                ih += authors[i];
                ih += "'";
            ih += " target='_blank'";
        ih += ">";
            ih += authors[i];
        ih += "</a>";
    }
    return ih + end;
}
function html_curr_instructions()
{
    var ih = "";
    ih += "<h2>" + L("We are mapping") + "</h2>";
    ih += "<ul>";
    for (var w in areas[curr_aid]["instructions"]) {
        ih += "<li>";
        if (areas[curr_aid]["instructions"][w].indexOf("http") != -1) {
            ih += "<a href='";
                    ih += areas[curr_aid]["instructions"][w];
                ih += "' target='_blank'>";
                ih += w;
            ih += "</a>";
        } else {
            ih += w + ": " + areas[curr_aid]["instructions"][w];
        }
        ih += "</li>";
    }
    ih += "</ul>";
    return ih;
}
function html_curr_actions()
{
    var ih = "";
    var commits = [];
    var whereami = "";
    if (curr_sid) {
        commits = curr_square_commits();
        whereami += "<p>";
        whereami += L("You are currently on square") + " " + curr_sid;
        whereami += " " + L("of area") + " " + curr_aid + ".";
        whereami += "</p>";
    }
    if (
        curr_sid
        && commits[0]["type"] == "locked"
        && commits[0]["aid"] == curr_aid
        && commits[0]["sid"] == curr_sid
    ) {
        ih += whereami;
        ih += html_open_id();
        ih += "<h2>" + L("Stop mapping and leave a message") + "</h2>";
        ih += "<textarea id='square_commit_text'></textarea>";
        if (!commits[1] || commits[1]["type"] == "to map") {
            ih += "<div";
                ih += " class='button'";
                ih += " onclick='unlock_map_map()'";
            ih += ">";
                ih += L("This square needs more mapping");
            ih += "</div>";
            ih += "<div";
                ih += " class='button'";
                ih += " onclick='unlock_map_split()'";
            ih += ">";
                ih += L("Split this square as it is huge");
            ih += "</div>";
            ih += "<div";
                ih += " class='button'";
                ih += " onclick='unlock_map_review()'";
            ih += ">";
                ih += L("Ready for review");
            ih += "</div>";
        } else if (commits[1]["type"] == "to review") {
            ih += "<div";
                ih += " class='button'";
                ih += " onclick='unlock_review_map()'";
            ih += ">";
                ih += L("This square needs more mapping");
            ih += "</div>";
            ih += "<div";
                ih += " class='button'";
                ih += " onclick='unlock_review_review()'";
            ih += ">";
                ih += L("Someone, please, review my review");
            ih += "</div>";
            ih += "<div";
                ih += " class='button'";
                ih += " onclick='unlock_review_done()'";
            ih += ">";
                ih += L("I approve this one as completed");
            ih += "</div>";
        }
    } else {
        if (curr_sid) {
            ih += whereami;
            ih += "<div";
                ih += " class='button'";
                ih += " onclick='goto_area(" + curr_aid + ", true)'";
            ih += ">";
                ih += L("Go back to area") + " " + curr_aid;
            ih += "</div>";
        }
        ih += "<h2>" + L("I will map") + "</h2>";
        ih += "<div";
            ih += " class='button'";
            ih += " onclick='map_random_square()'";
        ih += ">";
            ih += L("random square");
        ih += "</div>";
        ih += "<div";
            ih += " class='button'";
            ih += " onclick='map_recent_square()'";
        ih += ">";
            ih += L("recent square");
        ih += "</div>";
        ih += "<h2>" + L("I will review") + "</h2>";
        ih += "<div";
            ih += " class='button'";
            ih += " onclick='review_random_square()'";
        ih += ">";
            ih += L("random square");
        ih += "</div>";
        ih += "<div";
            ih += " class='button'";
            ih += " onclick='review_recent_square()'";
        ih += ">";
            ih += L("recent square");
        ih += "</div>";
    }
    return ih;
}
function html_wschat()
{
    var ih = "";
    ih += "<br />";
    ih += "&nbsp;<a href='javascript:show_div(\"wschat\")'>";
        ih += L("Show chat");
    ih += "</a>";
    ih += "&nbsp;|&nbsp;";
    ih += "<a href='javascript:hide_div(\"wschat\")'>";
        ih += L("Hide chat");
    ih += "</a>";
    ih += "<div id='wschat' style='display:none'>";
        ih += "<div id='messages' class='text w100'>";
            ih += L("Can't connect to wschat server") + ".";
        ih += "</div>";
        ih += "<div id='msg_div'><input";
            ih += " id='msg'";
            ih += " type='text'";
            ih += " class='w62'";
            ih += " onkeypress='send_msg(event)'";
            ih += " placeholder='";
                ih += L("Type a message here. Send by <Enter> key.");
                ih += "'";
        ih += "/></div>";
    ih += "</div>";
    return ih;
}
/* --- wschat functions --- */
function add_msg(msg)
{
    const messages = document.getElementById("messages");
    var d = new Date(msg["tim"]);
    messages.innerHTML += "<br />";
    messages.innerHTML += "<small>" + d.toLocaleTimeString() + "</small>";
    messages.innerHTML += " <em>" + msg["usr"] + "</em>:";
    messages.innerHTML += " " + msg["msg"];
    messages.scrollTop = messages.scrollHeight;
}

function send_msg(e)
{
    if (e.keyCode == 13) {
        const msg = document.getElementById("msg");
        var usr = load_user();
        var token = load_token();
        if (!usr || !token) {
            ws.onerror();
            return;
        }
        const t = msg.value;
        ws.send(JSON.stringify({
            "usr": usr["display_name"],
            "jwt": token,
            "rid": curr_aid,
            "msg": t
        }));
        msg.value = "";
    }
}
/* --- */
function html_curr_rate()
{
    var last = compute_curr_rate();
    var ih = "";
    ih += "<h2>"+ L("Mapping rate") + "</h2>";
    ih += "<ul>";
    ih += "<li>";
        ih += L("The area is mapped in") + " ";
        ih += Math.floor(
            areas[curr_aid]["#squares"] * 1 / last["mapping hour"]
        );
        ih += " " + L("hours when mapping") + " ";
        ih += last["mapping hour"]
        ih += " " + L("squares per hour") + ".";
    ih += "</li>";
    ih += "<li>";
        ih += L("The area is mapped in") + " ";
        ih += Math.floor(
            areas[curr_aid]["#squares"] * 3 / last["mapping 3hours"]
        );
        ih += " " + L("hours when mapping") + " ";
        ih += last["mapping 3hours"]
        ih += " " + L("squares per 3 hours") + ".";
    ih += "</li>";
    ih += "<li>";
        ih += L("The area is mapped in") + " ";
        ih += Math.floor(
            areas[curr_aid]["#squares"] * 24 / last["mapping day"]
        );
        ih += " " + L("hours when mapping") + " ";
        ih += last["mapping day"]
        ih += " " + L("squares per day") + ".";
    ih += "</li>";
    ih += "<li>";
        ih += L("The area is mapped in") + " ";
        ih += Math.floor(
            areas[curr_aid]["#squares"] * 24 * 7 / last["mapping week"]
        );
        ih += " " + L("hours when mapping") + " ";
        ih += last["mapping week"]
        ih += " " + L("squares per week") + ".";
    ih += "</li>";
    ih += "</ul>";
    ih += "<h2>" + L("Review rate") + "</h2>";
    ih += "<ul>";
    ih += "<li>";
        ih += L("The area is done in") + " ";
        ih += Math.floor(
            areas[curr_aid]["#squares"] * 1 / last["done hour"]
        );
        ih += " " + L("hours when reviewing") + " ";
        ih += last["done hour"]
        ih += " " + L("squares per hour") + ".";
    ih += "</li>";
    ih += "<li>";
        ih += L("The area is done in") + " ";
        ih += Math.floor(
            areas[curr_aid]["#squares"] * 3 / last["done 3hours"]
        );
        ih += " " + L("hours when reviewing") + " ";
        ih += last["done 3hours"]
        ih += " " + L("squares per 3 hours") + ".";
    ih += "</li>";
    ih += "<li>";
        ih += L("The area is done in") + " ";
        ih += Math.floor(
            areas[curr_aid]["#squares"] * 24 / last["done day"]
        );
        ih += " " + L("hours when reviewing") + " ";
        ih += last["done day"]
        ih += " " + L("squares per day") + ".";
    ih += "</li>";
    ih += "<li>";
        ih += L("The area is done in") + " ";
        ih += Math.floor(
            areas[curr_aid]["#squares"] * 24 * 7 / last["done week"]
        );
        ih += " " + L("hours when reviewing") + " ";
        ih += last["done week"]
        ih += " " + L("squares per week") + ".";
    ih += "</li>";
    ih += "</ul>";
    return ih;
}
function compute_curr_rate()
{
    var now = new Date();
    var added = [];
    var last = {
        "mapping hour": 0,
        "mapping 3hours": 0,
        "mapping day": 0,
        "mapping week": 0,
        "done hour": 0,
        "done 3hours": 0,
        "done day": 0,
        "done week": 0,
    };
    for (var i = 0; i < areas[curr_aid]["commits"].length; i++) {
        var commit = areas[curr_aid]["commits"][i];
        if (added.indexOf(commit["sid"]) != -1)
            continue;
        added.push(commit["sid"]);
        var cd = new Date(commit.date + "+00:00");
        if (now - 1 * 60 * 60 * 1000 < cd) {
            if (commit["type"] == "to review")
                last["mapping hour"] += 1;
            if (commit["type"] == "done")
                last["done hour"] += 1;
        }
        if (now - 3 * 60 * 60 * 1000 < cd) {
            if (commit["type"] == "to review")
                last["mapping 3hours"] += 1;
            if (commit["type"] == "done")
                last["done 3hours"] += 1;
        }
        if (now - 24 * 60 * 60 * 1000 < cd) {
            if (commit["type"] == "to review")
                last["mapping day"] += 1;
            if (commit["type"] == "done")
                last["done day"] += 1;
        }
        if (now - 7 * 24 * 60 * 60 * 1000 < cd) {
            if (commit["type"] == "to review")
                last["mapping week"] += 1;
            if (commit["type"] == "done")
                last["done week"] += 1;
        }
    }
    return last;
}
function squares_max_min()
{
    var max = [-9999, -9999];
    var min = [9999, 9999];
    for (var s in areas[curr_aid]["squares"]) {
        var square = areas[curr_aid]["squares"][s];
        var border = JSON.parse(square["border"])
        if (border["type"] != "Polygon")
            continue;
        for (var c in border["coordinates"][0]) {
            if (border["coordinates"][0][c][0] > max[0])
                max[0] = border["coordinates"][0][c][0];
            if (border["coordinates"][0][c][0] < min[0])
                min[0] = border["coordinates"][0][c][0];
            if (border["coordinates"][0][c][1] > max[1])
                max[1] = border["coordinates"][0][c][1];
            if (border["coordinates"][0][c][1] < min[1])
                min[1] = border["coordinates"][0][c][1];
        }
    }
    return [max, min];
}
function html_curr_score()
{
    var score = compute_curr_score();
    var ih = "";
    ih += "<h2>" + L("Mappers score") + "</h2>";
    ih += "<div class='column'>";
    ih += "<h3>" + L("Mapped") + "</h2>";
    for (var u in score["mapped"]) {
        if (score["mapped"][u][0] == 0)
            continue;
        ih += score["mapped"][u][1] + ": " + score["mapped"][u][0];
        ih += " " + L("squares");
        ih += "<br />";
    }
    ih += "</div>";
    ih += "<div class='column'>";
    ih += "<h3>" + L("Reviewed") + "</h2>";
    for (var u in score["reviewed"]) {
        if (score["reviewed"][u][0] == 0)
            continue;
        ih += score["reviewed"][u][1] + ": " + score["reviewed"][u][0];
        ih += " " + L("squares");
        ih += "<br />";
    }
    ih += "</div>";
    return ih;
}
function compute_curr_score(since=false)
{
    var users = {};
    for (var i = 0; i < areas[curr_aid]["commits"].length - 2; i++) {
        var commit = areas[curr_aid]["commits"][i];
        if (commit["type"] == "locked")
            continue;
        if (!users[commit["author"]]) {
            users[commit["author"]] = {
                "mapped": 0,
                "reviewed": 0,
            };
        }
        if (
            commit["type"] == "done"
            && commit["message"] != "The square was splitted"
        ) {
            users[commit["author"]]["reviewed"] += 1;
        } else if (
            commit["type"] == "to map"
            && i + 2 < areas[curr_aid]["commits"].length
            && areas[curr_aid]["commits"][i + 2]["type"] == "to review"
            && commit["message"] != "Unlock automatically due to timeout"
        ) {
            users[commit["author"]]["reviewed"] += 1;
        } else if (commit["type"] == "to review") {
            users[commit["author"]]["mapped"] += 1;
        }
        if (since && since < commit["date"])
            break;
    }
    var mapped_score = [];
    var reviewed_score = [];
    for (var u in users) {
        mapped_score.push([users[u]["mapped"], u]);
        reviewed_score.push([users[u]["reviewed"], u]);
    }
    mapped_score.sort(function(a, b) { return b[0] - a[0]; });
    reviewed_score.sort(function(a, b) { return b[0] - a[0]; });
    return {
        "mapped": mapped_score,
        "reviewed": reviewed_score,
    };
}
function html_squares_map()
{
    var mm = squares_max_min();
    var scale = document.getElementById("squares_map").offsetWidth;
    scale /= mm[0][0] - mm[1][0];
    var ih = "";
    ih += "<h2>" + L("Squares map") + "</h2>";
    ih += "<svg width='100%' height='" + (mm[0][1] - mm[1][1]) * scale + "'>";
    for (var s in areas[curr_aid]["squares"]) {
        var square = areas[curr_aid]["squares"][s];
        var border = JSON.parse(square["border"])
        if (border["type"] != "Polygon")
            continue;
        ih += "<polygon";
            ih += " id='square" + square["sid"] + "'";
            ih += " points='";
            for (var c in border["coordinates"][0]) {
                ih += (border["coordinates"][0][c][0] - mm[1][0]) * scale;
                ih += ",";
                ih += (
                    (mm[0][1] - mm[1][1]) * scale
                    - (border["coordinates"][0][c][1] - mm[1][1]) * scale
                );
                ih += " ";
            }
            ih += "'";
            ih += " style='fill:gray;stroke:#000000;stroke-width:1px;'";
        ih += ">";
            ih += "<title id='square" + square["sid"] + "title'>";
                ih += L("Square") + " " + square["sid"] + " " + L("to map");
            ih += "</title>";
        ih += "</polygon>";
    }
    ih += "</svg>";
    return ih;
}

// -- square --
function goto_square(aid, sid)
{
    window.scrollTo(0, 0);
    reset_globals();
    curr_aid = aid;
    curr_sid = sid;
    get_commits(store_commits, curr_aid, curr_lcd());
    up("actions", html_curr_actions());
    up("authors_h", ((curr_sid)?L("Square"):L("Area")) + " " + L("commits"));
    if (document.getElementById("commits").innerHTML != "")
        up("commits", html_curr_commits());
}
// -- square -- html procedures
function curr_square_commits()
{
    var commits = [];
    for (var c in areas[curr_aid]["commits"]) {
        var commit = areas[curr_aid]["commits"][c];
        if (commit["sid"] != curr_sid)
            continue;
        commits.push(commit);
    }
    return commits;
}
function compute_area_tags()
{
    var tags = areas[curr_aid]["tags"].replace(/[^a-zA-Z0-9- ]/g, "");
    var tags_array = tags.split(" ");
    tags = "";
    tags += "#" + curr_aid
    for (var i in tags_array)
        tags += " #" + tags_array[i];
    return tags;
}
function open_id_url(s)
{
    var url = "https://www.openstreetmap.org/edit?editor=id";
    var imagery = "Maxar-Premium";
    var clon;
    var clat;
    if (
        s["border"]["type"] == "Polygon"
        && s["border"]["coordinates"][0].length > 2
    ) {
        clon = (
            s["border"]["coordinates"][0][0][0]
            + s["border"]["coordinates"][0][2][0]
        ) / 2;
        clat = (
            s["border"]["coordinates"][0][0][1]
            + s["border"]["coordinates"][0][2][1]
        ) / 2;
    } else if (
        s["border"]["type"] == "LineString"
        && s["border"]["coordinates"].length > 2
    ) {
        clon = (
            s["border"]["coordinates"][0][0]
            + s["border"]["coordinates"][2][0]
        ) / 2;
        clat = (
            s["border"]["coordinates"][0][1]
            + s["border"]["coordinates"][2][1]
        ) / 2;
    } else {
        var msg = "The square has bad geometry.";
        var u = load_user();
        if (u["tags"] != "")
            msg += " ~" + u["tags"];
        post_square_commit(
            function (r) {
                goto_area(curr_aid, true);
            },
            {
                "aid": curr_aid,
                "sid": curr_sid,
                "author": u["display_name"],
                "type": "done",
                "message": msg,
            },
        );
        return false;
    }
    url += "&#map=17/" + clat + "/" + clon;
    //url += "&background=Maxar-Premium";
    url += "&comment=" + encodeURI(compute_area_tags());
    //url += "&hashtags=" + encodeURI("area tags <-- TODO");
    //url += "&source=" + imagery;
    url += "&gpx=" + encodeURIComponent(
        DAMN_SERVER
        + "/area/" + s["aid"]
        + "/square/" + s["sid"]
        + "/gpx"
    );
    return url;
}
function open_id(s)
{
    var url = open_id_url(s);
    if (url)
        window.open(url);
}
function html_open_id()
{
    var ih = "";
    ih += "<div";
        ih += " class='button'";
        ih += " onclick='map_random_square()'"; // TODO this is workaround*
    ih += ">";
        ih += L("Open iD editor again");
    ih += "</div>";
    return ih;
// *I call it workaround because here we call API to map random square again.
// But the server returns users's currently locked square.
}

// -- area -- post commit actions
function map_random_square()
{
    post_area_commit(
        function (r) {
            goto_square(r["aid"], r["sid"]);
            open_id(r);
        },
        {
            "aid": curr_aid,
            "type": "map",
            "what": "random",
        },
    );
}
function map_recent_square()
{
    post_area_commit(
        function (r) {
            goto_square(r["aid"], r["sid"]);
            open_id(r);
        },
        {
            "aid": curr_aid,
            "type": "map",
            "what": "recent",
        },
    );
}
function review_random_square()
{
    post_area_commit(
        function (r) {
            goto_square(r["aid"], r["sid"]);
            open_id(r);
        },
        {
            "aid": curr_aid,
            "type": "review",
            "what": "random",
        },
    );
}
function review_recent_square()
{
    post_area_commit(
        function (r) {
            goto_square(r["aid"], r["sid"]);
            open_id(r);
        },
        {
            "aid": curr_aid,
            "type": "review",
            "what": "recent",
        },
    );
}

// -- square -- post commit actions
function unlock_map_map()
{
    var msg = document.getElementById("square_commit_text").value;
    if (msg == "")
        msg = L("This square needs more mapping") + ".";
    var u = load_user();
    if (u["tags"] != "")
        msg += " ~ " + u["tags"];
    post_square_commit(
        function (r) {
            goto_area(curr_aid, true);
        },
        {
            "aid": curr_aid,
            "sid": curr_sid,
            "author": u["display_name"],
            "type": "to map",
            "message": msg,
        },
    );
}
function unlock_map_split()
{
    var msg = document.getElementById("square_commit_text").value;
    if (msg == "")
        msg = L("I had to split this as no human could map it alone.");
    var u = load_user();
    if (u["tags"] != "")
        msg += " ~" + u["tags"];
    post_square_commit(
        function (r) {
            goto_area(curr_aid, true);
        },
        {
            "aid": curr_aid,
            "sid": curr_sid,
            "author": u["display_name"],
            "type": "splitted",
            "message": msg,
        },
    );
}
function unlock_map_review()
{
    var msg = document.getElementById("square_commit_text").value;
    if (msg == "")
        msg = L("This square is ready for review. I swear.");
    var u = load_user();
    if (u["tags"] != "")
        msg += " ~" + u["tags"];
    post_square_commit(
        function (r) {
            goto_area(curr_aid, true);
        },
        {
            "aid": curr_aid,
            "sid": curr_sid,
            "author": u["display_name"],
            "type": "to review",
            "message": msg,
        },
    );
}
function unlock_review_map()
{
    var msg = document.getElementById("square_commit_text").value;
    if (msg == "")
        msg = L("This square needs a little bit more mapping.");
    var u = load_user();
    if (u["tags"] != "")
        msg += " ~" + u["tags"];
    post_square_commit(
        function (r) {
            goto_area(curr_aid, true);
        },
        {
            "aid": curr_aid,
            "sid": curr_sid,
            "author": u["display_name"],
            "type": "to map",
            "message": msg,
        },
    );
}
function unlock_review_review()
{
    var msg = document.getElementById("square_commit_text").value;
    if (msg == "")
        msg = L("Please, review my review someone. Thanks.");
    var u = load_user();
    if (u["tags"] != "")
        msg += " ~" + u["tags"];
    post_square_commit(
        function (r) {
            goto_area(curr_aid, true);
        },
        {
            "aid": curr_aid,
            "sid": curr_sid,
            "author": u["display_name"],
            "type": "to review",
            "message": msg,
        },
    );
}
function unlock_review_done()
{
    var msg = document.getElementById("square_commit_text").value;
    if (msg == "")
        msg = L("Yay! This square is completely done.");
    var u = load_user();
    if (u["tags"] != "")
        msg += " ~" + u["tags"];
    post_square_commit(
        function (r) {
            goto_area(curr_aid, true);
        },
        {
            "aid": curr_aid,
            "sid": curr_sid,
            "author": u["display_name"],
            "type": "done",
            "message": msg,
        },
    );
}

// -- user --
function html_logout_user()
{
    return "<a href='javascript:logout_user()'>" + L("logout") + "</a>";
}
function html_user_stats(stats)
{
    var ih = "";
    ih += html_logout_user();
    ih += "&nbsp;|&nbsp;";
    ih += "<a href='javascript:load_areas()'>" + L("load areas") + "</a>";
    var u = load_user();
    ih += "<h2>" + u["display_name"];
    ih += " " + L("and last week statistics");
    ih += "</h2>";
    ih += "<div class='column'>";
    ih += "<h3>" + L("Mapped") + "</h2>";
    var total = 0;
    for (var a in stats["mapped"]) {
        ih += a + ": " + stats["mapped"][a].toFixed(3) + " " + L("hours");
        ih += "<br />";
        total += stats["mapped"][a];
    }
    ih += "<br />" + L("Total") + ": " + total.toFixed(3);
    ih += "</div>";
    ih += "<div class='column'>";
    ih += "<h3>" + L("Reviewed") + "</h2>";
    total = 0;
    for (var a in stats["reviewed"]) {
        ih += a + ": " + stats["reviewed"][a].toFixed(3) + " " + L("hours");
        ih += "<br />";
        total += stats["reviewed"][a];
    }
    ih += "<br />" + L("Total") + ": " + total.toFixed(3);
    ih += "</div>";
    return ih;
}
function tdiff(t_new, t_old)
{
    var tn = new Date(t_new);
    var to = new Date(t_old);
    return (tn.getTime() - to.getTime()) / 1000 / 60 / 60;
}
function compute_user_stats(commits)
{
    var stats = {};
    stats["commits"] = commits.length;
    stats["mapped"] = {};
    stats["reviewed"] = {};
    if (commits.length < 2)
        return stats;
    if (commits[commits.length - 1]["type"] == "locked") {
        var lc = commits[commits.length - 1];
        var c = commits[commits.length - 2];
        if (c["type"] == "done") {
            if (!stats["reviewed"][c["aid"]])
                stats["reviewed"][c["aid"]] = tdiff(c["date"], lc["date"]);
            else
                stats["reviewed"][c["aid"]] += tdiff(c["date"], lc["date"]);
        }
        if (c["type"] == "to review" || c["type"] == "to map") {
            if (!stats["mapped"][c["aid"]])
                stats["mapped"][c["aid"]] = tdiff(c["date"], lc["date"]);
            else
                stats["mapped"][c["aid"]] += tdiff(c["date"], lc["date"]);
        }
    }
    for (var i = 0; i < commits.length - 2; i++) {
        var c = commits[i];
        var lc = commits[i + 1];
        var llc = commits[i + 2];
        if (!c["sid"])
            continue;
        if (c["message"] == "Unlock automatically due to timeout")
            continue;
        if (lc["type"] != "locked")
            continue;
        if (
            (c["type"] == "done" && llc["type"] == "to review")
            || (c["type"] == "to map" && llc["type"] == "to review")
            || (c["type"] == "to review" && llc["type"] == "to review")
        ) {
            if (!stats["reviewed"][c["aid"]])
                stats["reviewed"][c["aid"]] = tdiff(c["date"], lc["date"]);
            else
                stats["reviewed"][c["aid"]] += tdiff(c["date"], lc["date"]);
        }
        if (
            (c["type"] == "to review" && llc["type"] == "to map")
            || (c["type"] == "to map" && llc["type"] == "to map")
        ) {
            if (!stats["mapped"][c["aid"]])
                stats["mapped"][c["aid"]] = tdiff(c["date"], lc["date"]);
            else
                stats["mapped"][c["aid"]] += tdiff(c["date"], lc["date"]);
        }
    }
    return stats;
}
function load_user_stats()
{
    up("working", L("Loading data") + "...");
    var u = load_user();
    var d = new Date();
    d = new Date(d - 7 * 24 * 60 * 60 * 1000);
    get_commits(function (commits) {
        up("main", html_user_stats(compute_user_stats(commits)));
        up("working", "");
    }, u["display_name"], d.toISOString());
}
function show_user()
{
    load_user_stats();
}
function logout_user()
{
    delete_token();
    load_areas();
}
function change_language(lang)
{
    LANG=lang;
    load_areas();
}
